package com.ingress.az.Task4.service;

import com.ingress.az.Task4.model.Car;
import lombok.Data;

import java.util.List;
public interface CarService {


    Car getById(Integer id);
    Car update(Integer id, Car car);
    Car create(Car car);
    void delete(Integer id);
    List <Car> getAllCar();


}

package com.ingress.az.Task4.Repository;

import com.ingress.az.Task4.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car,Integer> {
}

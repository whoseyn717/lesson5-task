package com.ingress.az.Task4.controller;

import com.ingress.az.Task4.model.Car;
import com.ingress.az.Task4.service.CarService;
import com.ingress.az.Task4.service.CarServiceImpl;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Car")
public class CarController {
    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }
    @GetMapping("/{id}")
    public Car get(@PathVariable Integer id){
        return carService.getById(id);
    }
    @GetMapping("/all")
    public List<Car> getAll() {
        return carService.getAllCar();
    }
    @PostMapping("/create")
    public Car create(@RequestBody Car car){
        return carService.create(car);
    }
    @PutMapping("/update/{id}")
    public Car update(@PathVariable Integer id, @RequestBody Car car){
        return carService.update(id,car);
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable Integer id){
        carService.delete(id);

    }





}

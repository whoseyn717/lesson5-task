package com.ingress.az.Task4.service;

import com.ingress.az.Task4.Repository.CarRepository;
import com.ingress.az.Task4.model.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@Slf4j
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car getById(Integer id) {
        Optional<Car> car = carRepository.findById(id);
        if (car.isEmpty()) {
            throw new RuntimeException("Car not found with id: "+id);
        }
        return car.get();

    }
    @Override
    public Car create(Car car) {
        Car carInDb = carRepository.save(car);
        return carInDb;

    }
@Override
    public Car update(Integer id, Car car) {
        Car carUp = carRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Car not found with id: "+id));
        carUp.setColor(car.getColor());
        carUp.setPrice(car.getPrice());
        carUp.setModelYear(car.getModelYear());
        carUp.setModel(car.getModel());
        carUp.setCondition(car.isCondition());
        return carRepository.save(carUp);
    }
@Override
    public void delete(Integer id){
        carRepository.deleteById(id);
    }

    @Override
    public List<Car> getAllCar() {
        return carRepository.findAll();
    }
}
